console.log('hello world');

(function()
{
    const MAIN_OBJ = {
        init: function () {
            console.log("Cargando JavaScript");
            this.eventHeadlers();
        },

        eventHeadlers: function (){
            document.querySelector(".hamburguer-icon").addEventListener("click", function(){
                document.querySelector(".menu-container").classList.toggle("menu-open");
            });

            document.querySelector(".sub-hamburguer-icon").addEventListener("click", function(){
                document.querySelector(".sub-menu-container").classList.toggle("sub-menu-open");
            });
        }
    }
    MAIN_OBJ.init();
})();
