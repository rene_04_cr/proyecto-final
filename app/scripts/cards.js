console.log('Cargando Tarjetas')
const dataCards = [
    {
        "title": "Juega solo o con amigos",
        "url_image":"https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/11/15/18/league-of-legends.jpg?w968h681",
        "desc":"No es necesario ser parte de un equipo, aunque no está de más un poco de ayuda.",
        "cta":"Show More",
        "link":"https://www.edsurge.com/news/2019-01-22-educators-share-how-video-games-can-help-kids-build-sel-skills"
      },
      {
        "title": "Juegos Clasicos",
        "url_image":"https://i.ytimg.com/vi/Si6QPKiNoDY/maxresdefault.jpg",
        "desc":"Puedes jugar solo o en linea",
        "cta":"Show More",
        "link":"https://www.monederosmart.com/wp-content/uploads/2020/02/0-Tecaldo-gaming-122324698_m.jpg"
      },
      {
        "title": "Juega en linea con jugadores de cualquier parte del mundo",
        "url_image":"https://a.engagesciences.com/images/5881/5ba89345-4ad5-40bd-8d2f-8ab3eee28fad/Versus.FB.1640x624.V2.jpg",
        "desc":"No es necesario ser parte de un equipo, aunque no está de más un poco de ayuda.",
        "cta":"Show More",
        "link":"https://www.internetmatters.org/es/resources/online-gaming-advice/the-basics/"
      },
      {
        "title": "Que es el E-Sports",
        "url_image":"https://www.futuresplatform.com/sites/default/files/styles/article_hero_image/public/2019-02/esports.jpg?itok=TK6lpK_V",
        "desc":"No es necesario ser parte de un equipo, aunque no está de más un poco de ayuda.",
        "cta":"Show More",
        "link":"https://www.iebschool.com/blog/que-es-esports-marketing-digital/"
      },
      {
        "title": "Multiples manera de jugar",
        "url_image":"https://elandroidelibre.elespanol.com/wp-content/uploads/2015/06/Vainglory1.jpg",
        "desc":"No es necesario ser parte de un equipo, aunque no está de más un poco de ayuda.",
        "cta":"Show More",
        "link":"https://www.esportsbureau.com/wp-content/uploads/2019/06/Presentacio%CC%81n_WINKTTD_Descubriendo-los-ESports.pdf"
      },
      {
        "title": "ESports en dispositivos moviles",
        "url_image":"https://www.hd-tecnologia.com/imagenes/articulos/2020/09/Los-esports-en-dispositivos-m%C3%B3viles-generaron-19.5-mil-millones-de-d%C3%B3lares-en-2019-y-superar%C3%A1-a-la-PC-1280x720.jpg",
        "desc":"No es necesario ser parte de un equipo, aunque no está de más un poco de ayuda.",
        "cta":"Show More",
        "link":"https://www.redbull.com/es-es/esports-en-moviles-juegos-consolidados-futuro"
      },
];

(function(){
    let CARD = {
      init: function(){
        console.log('card module was loaded');
        let _self = this;

        //llamamos las funciones
        this.insertData(_self);
      },

      eventHandler: function (_self) {
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function(event){
                    console.log('event', event);
                    _self.showTab(event.target);
                });                
            }
        },

        insertData: function (_self){
            dataCards.map(function (item, index){
                document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
            });
        },

        tplCardItem: function(item, index){
          return(`<div class='card-item' id="card-number-${index}">
          <img src="${item.url_image}"/>
          <div class="card-info">
            <p class='card-title'>${item.title}</p>
            <p class='card-desc'>${item.desc}</p>
            <a class='card-cta' target="blank" href="${item.link}">${item.cta}</a>
          </div>
        </div>`)},
    }
    CARD.init();
})();
